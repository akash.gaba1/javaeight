package com.wheelseye.javaeight.functional.available;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Run {
    public static void main(String[] args) {
        final Function<Integer, Integer> square = integer ->  integer * integer;
        System.out.println(square.apply(2));
        final Function<Integer, Integer> toPower4 = square.andThen(square);
        System.out.println(toPower4.apply(2));
        final Consumer<Object> print = System.out::println;
        print.accept("hello1");
        final Supplier<String> hello = () -> "Hello";
        System.out.println(hello.get());
        final Runnable printer = () -> System.out.println("runnable");
        printer.run();

    }
}
