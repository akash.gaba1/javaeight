package com.wheelseye.javaeight.functional;

public class Run {
    public static void main(String[] args) {
        IStringConcat iStringConcat = new IStringConcat() {
            @Override
            public String concat(String str, String str2) {
                return str + str2;
            }
        };
        System.out.println("Annonymous class : " + iStringConcat.concat("test1", "test1"));
        IStringConcat iStringConcat2 = (str, str2) -> str + str2;
        System.out.println("lambda : " + iStringConcat2.concat("test2", "test2"));
        FIStringConcat fiStringConcat = (str, str2) -> str + str2;
        System.out.println("lambda for fi : " + fiStringConcat.concat("test3", "test3"));
        System.out.println("default method : " + fiStringConcat.defaultConcat("test4", "test4"));
    }
}
