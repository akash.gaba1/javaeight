package com.wheelseye.javaeight.functional;

@FunctionalInterface
public interface FIStringConcat {
    String concat(String str, String str2);

    default String defaultConcat(String str, String str2) {
        return str + " " + str2;
    }
}
