package com.wheelseye.javaeight.optional;

import java.util.List;
import java.util.Optional;

public class Run {
    public static void main(String[] args) {



        A a = A.sample1();
        print("0th: " + Optional.ofNullable(a)
                .map(A::getB)
                .map(A.B::getCs)
                .filter(cs -> cs.size() > 0)
                .map(cs -> cs.get(0))
                .map(A.C::getI)
                .orElse(-1));

        print("4th: " + Optional.ofNullable(a)
                .map(A::getB)
                .map(A.B::getCs)
                .filter(cs -> cs.size() > 4)
                .map(cs -> cs.get(4))
                .map(A.C::getI)
                .orElse(-1));

        print("2nd: " + Optional.ofNullable(a)
                .map(A::getB)
                .map(A.B::getCs)
                .flatMap(Run::get2nd)
                .orElse(-1));
    }

    private static void print(Object o) {
        System.out.println(o);
    }

    private static Optional<Integer> get2nd(List<A.C> cs) {
        return Optional.ofNullable(cs)
                .filter(csLocal -> csLocal.size() > 2)
                .map(cs1 -> cs1.get(2))
                .map(A.C::getI);
    }
}
