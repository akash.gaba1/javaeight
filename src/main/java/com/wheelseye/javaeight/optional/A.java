package com.wheelseye.javaeight.optional;

import java.util.Arrays;
import java.util.List;

public class A {
    private B b;

    public static A sample1() {
        return new A().setB(B.sample1());
    }

    public B getB() {
        return b;
    }

    public A setB(B b) {
        this.b = b;
        return this;
    }

    public static class B {
        private List<C> cs;

        public static B sample1() {
            return new B().setCs(
                    Arrays.asList(
                            C.of(1),
                            C.of(2),
                            C.of(3),
                            C.of(4)
                    )
            );
        }

        public List<C> getCs() {
            return cs;
        }

        public B setCs(List<C> cs) {
            this.cs = cs;
            return this;
        }
    }

    public static class C {
        private Integer i;

        public static C of(Integer i) {
            return new C().setI(i);
        }

        public Integer getI() {
            return i;
        }

        public C setI(Integer i) {
            this.i = i;
            return this;
        }
    }
}
