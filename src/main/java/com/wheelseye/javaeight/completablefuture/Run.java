package com.wheelseye.javaeight.completablefuture;

import java.util.concurrent.CompletableFuture;

public class Run {
    public static void main(String[] args) {

        CompletableFuture<Void> task1 = CompletableFuture.supplyAsync(() -> 5)
                .thenApplyAsync(Run::squareWait)
                .thenAcceptAsync(Run::print)
                .thenRunAsync(() -> print("\n"));
//                .thenApply(Run::squareWait)
//                .thenAccept(Run::print)
//                .thenRun(() -> print("\n"));

        CompletableFuture<Void> task2 = CompletableFuture.supplyAsync(() -> 10)
                .thenApplyAsync(Run::square)
                .thenAcceptAsync(Run::print)
                .thenRunAsync(() -> print("\n"));
//                .thenApply(Run::square)
//                .thenAccept(Run::print)
//                .thenRun(() -> print("\n"));
        task1.join();
        task2.join();
    }

    static void print(Object o) {
        System.out.print(o);
    }

    static Integer square(Integer integer) {
        return integer * integer;
    }

    static Integer squareWait(Integer integer) {
        sleep(1000);
        return integer * integer;
    }

    static void sleep(long mili) {
        try {
            Thread.sleep(mili);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
