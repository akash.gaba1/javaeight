package com.wheelseye.javaeight.completablefuture.combine;

import java.util.concurrent.CompletableFuture;

public class Run {
    public static void main(String[] args) {
        CompletableFuture<Integer> square = CompletableFuture.supplyAsync(() -> square(2));
        CompletableFuture<Integer> cube = CompletableFuture.supplyAsync(() -> cube(3));
        CompletableFuture<Integer> sum = cube.thenCombineAsync(square, Integer::sum);
        square.thenAcceptAsync(Run::print);
        CompletableFuture<Void> printSum = sum.thenAcceptAsync(Run::print);
        cube.thenAcceptAsync(Run::print);
        printSum.join();
    }

    static Integer square(Integer integer) {
        sleep(100);
        return integer * integer;
    }

    static Integer cube(Integer integer) {
        sleep(10);
        return integer * integer * integer;
    }

    static void print(Object o) {
        System.out.print(o + "\n");
    }

    static void sleep(long mili) {
        try {
            Thread.sleep(mili);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
