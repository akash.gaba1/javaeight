package com.wheelseye.javaeight.completablefuture.compose;

import java.util.concurrent.CompletableFuture;

public class Run {
    public static void main(String[] args) {
        CompletableFuture<Integer> task = CompletableFuture.supplyAsync(Run::getNum)
                .thenComposeAsync(Run::square);
        print("main thread");
        task.join();

    }

    static CompletableFuture<Integer> square(Integer integer) {
        return CompletableFuture.supplyAsync(() -> {
            sleep(100);
            print("get square");
            return integer * integer;
        });
    }

    static Integer getNum() {
        sleep(10);
        print("get num");
        return 10;
    }

    static void print(Object o) {
        System.out.print(o + "\n");
    }

    static void sleep(long mili) {
        try {
            Thread.sleep(mili);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
