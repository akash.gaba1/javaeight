package com.wheelseye.javaeight.completablefuture.exception;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        CompletableFuture.completedFuture(10)
                .thenApply(Run::printAndReturnInput)
                .thenApply(Run::square)
                .thenApply(Run::printAndReturnInput)
                .thenApply(Run::fail)
                .thenApply(Run::printAndReturnInput)
                .thenApply(Run::square)
                .thenApply(Run::printAndReturnInput)
                .exceptionally(Run::save)
                .thenApply(Run::printAndReturnInput)
                .thenApply(Run::fail)
                .join();

    }

    static Integer square(Integer integer) {
        return integer * integer;
    }

    static Integer fail(Integer integer) {
        throw new IllegalArgumentException("" + integer);
    }

    static Integer save(Throwable throwable) {
        print(trace(throwable.getStackTrace()));
        print(trace(throwable.getCause().getStackTrace()));
        return 5;
    }

    static String trace(StackTraceElement[] stackTraceElements) {
        return Stream.of(stackTraceElements).map(Objects::toString).map(s -> s + "\n").collect(Collectors.toList()).toString();
    }

    static void print(Object o) {
        System.out.println(o);
    }

    static Integer printAndReturnInput(Integer integer) {
        print(integer);
        return integer;
    }
}
