package com.wheelseye.javaeight.stream.collect;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        List<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 6, 6, 7, 8, 8, 9, 10, 10)
                .collect(Collectors.toList());

        print("list:" + integers.stream().collect(Collectors.toList()));

        print("set:" + integers.stream().collect(Collectors.toSet()));

        print("value and list:" + integers.stream().collect(Collectors.groupingBy(Function.identity())));

        print("value and count occurrence:" + integers.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));

        print("value and count occurrence:" + integers.stream().collect(Collectors.collectingAndThen(Collectors.toSet(), Run::format)));

    }

    static void print(Object o) {
        System.out.println(o + "\n");
    }

    static String format(Set<Integer> integers) {
        return Optional.ofNullable(integers)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(Objects::toString)
                .map(s -> String.format("\"%s\"", s))
                .reduce((str1, str2) -> String.format("%s,%s", str1, str2))
                .orElse("");
    }
}
