package com.wheelseye.javaeight.stream;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        List<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 6, 6, 7, 8, 8, 9, 10, 10)
                .collect(Collectors.toList());

        print("print square: " + integers.stream()
                .map(integer -> integer * integer)
                .collect(Collectors.toList()));

        print("print square distinct: " + integers.stream()
                .map(integer -> integer * integer)
                .distinct()
                .collect(Collectors.toList()));

        print("print square reverse sorted: " + integers.stream()
                .map(integer -> integer * integer)
                .distinct()
                .sorted(Comparator.<Integer, Integer>comparing(i -> i).reversed())
                .collect(Collectors.toList()));

        print("findAny greater than 5 findAny: " + integers.stream()
                .filter(integer -> integer > 5)
                .findAny());

        print("find first greater than 5: " + integers.stream()
                .filter(integer -> integer > 5)
                .findFirst());

        print("find first greater than 10: " + integers.stream()
                .filter(integer -> integer > 10)
                .findFirst());

        print("count distinct: " + integers.stream()
                .distinct()
                .count());

        print("flat map: " + Stream.of(odd(), even())
                .flatMap(Collection::stream)
                .collect(Collectors.toList()));
    }

    private static List<Integer> odd() {
        return Stream.of(1, 3, 5, 7)
                .collect(Collectors.toList());
    }

    private static List<Integer> even() {
        return Stream.of(0, 2, 4, 6)
                .collect(Collectors.toList());
    }

    static void print(Object o) {
        System.out.println(o + "\n");
    }
}
