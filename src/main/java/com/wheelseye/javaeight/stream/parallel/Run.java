package com.wheelseye.javaeight.stream.parallel;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        List<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 6, 6, 7, 8, 8, 9, 10, 10)
                .collect(Collectors.toList());
//        print((long) integers.size());
//
//        print("normal Loop:");
//        executionTime(() -> {
//            for (Integer integer : integers) {
//                printWithThread(integer);
//            }
//        });
        print("forEach:");
        executionTime (() -> integers.stream().forEach(Run::printWithThread));
        print("parallel().forEach:");
        executionTime (() -> integers.stream().parallel().forEach(Run::printWithThread));
        print("parallelStream().forEach:");
        executionTime (() -> integers.parallelStream().forEach(Run::printWithThread));
        print("parallelStream().sequential().forEach:");
        executionTime (() -> integers.parallelStream().sequential().forEach(Run::printWithThread));
    }

    static void printWithThread(Object o) {
//        sleep(10);
//        print(Thread.currentThread() + ":" + o);
    }

    static void print(Object o) {
        System.out.print(o + "\n");
    }

    static void executionTime(Runnable runnable) {
        LocalTime start = LocalTime.now();
        runnable.run();
        LocalTime end = LocalTime.now();
        print(end.minusHours(start.getHour()).minusMinutes(start.getMinute()).minusSeconds(start.getSecond()).minusNanos(start.getNano()).getNano());
    }

    static void sleep(long mili) {
        try {
            Thread.sleep(mili);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
