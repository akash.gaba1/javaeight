package com.wheelseye.javaeight.stream.parallel.terminaloperator;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        List<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 6, 6, 7, 8, 8, 9, 10, 10)
                .collect(Collectors.toList());

        print("forEach\n");
        integers.parallelStream().forEach(Run::print);
        print("\n");
        integers.parallelStream().forEach(Run::print);
        print("\n");
        integers.parallelStream().forEach(Run::print);
        print("\n");
        print("forEachOrdered\n");
        integers.parallelStream().forEachOrdered(Run::print);
        print("\n");
        integers.parallelStream().forEachOrdered(Run::print);
        print("\n");
        integers.parallelStream().forEachOrdered(Run::print);
        print("\n");
        print("findFirst\n");
        integers.parallelStream().findFirst().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findFirst().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findFirst().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findFirst().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findFirst().ifPresent(Run::print);
        print("\n");
        print("findAny\n");
        integers.parallelStream().findAny().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findAny().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findAny().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findAny().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findAny().ifPresent(Run::print);
        print("\n");
        integers.parallelStream().findAny().ifPresent(Run::print);
        print("\n");
        print("parallelStream().reduce\n");
        integers.parallelStream().reduce(Run::sum).ifPresent(Run::print);
        print("\n");
        integers.parallelStream().reduce(Run::sum).ifPresent(Run::print);
        print("\n");
        print("stream().reduce\n");
        integers.stream().reduce(Run::sum).ifPresent(Run::print);
        print("\n");
        integers.stream().reduce(Run::sum).ifPresent(Run::print);
    }
    static void print(Object o) {
        System.out.print(o);
    }
    static Integer sum(Integer i1, Integer i2) {
        print(i1 + ":" + i2 + ",");
//        sleep(10);
        return i1 / i2;
    }
    static void sleep(long mili) {
        try {
            Thread.sleep(mili);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
