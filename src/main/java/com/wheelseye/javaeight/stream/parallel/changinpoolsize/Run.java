package com.wheelseye.javaeight.stream.parallel.changinpoolsize;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        List<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 6, 6, 7, 8, 8, 9, 10, 10)
                .collect(Collectors.toList());
        integers.forEach(Run::print);
        print("");
        integers.parallelStream().forEach(Run::print);
        print("");
        new ForkJoinPool(50).submit(() -> integers.parallelStream().forEach(Run::print)).join();
        print("");
        print(ForkJoinPool.commonPool());
        print("");
        ForkJoinPool.commonPool().submit(() -> integers.parallelStream().forEach(Run::print)).join();

    }

    static void print(Object o) {
        System.out.print(Thread.currentThread() + ":" + o + "\n");
    }
}
